//
//  Doer.swift
//  Closures Example
//
//  Created by Jason Henderson on 9/22/15.
//  Copyright © 2015 CSU Monterey Bay. All rights reserved.
//

import UIKit

func / (left: Int, right: Int) -> Float {
    return Float(left) / Float(right)
}

func % (left: Float, right: Float) -> Int {
    return Int(left * 100) % Int(right * 100)
}

class Doer: NSObject {

    /// This function returns a *hello* string for a given `subject`.
    ///
    /// **Warning:** The returned string is not localized.
    ///
    /// Usage:
    ///
    ///    println(hello("Markdown")) // Hello, Markdown!
    ///
    /// :param: subject The subject to be welcomed.
    ///
    /// :returns: A hello string to the `subject`.
    func doSomethingOnServer(callback: ((Int, Float) -> Int)?) {
        
        let priority = DISPATCH_QUEUE_PRIORITY_BACKGROUND
        
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            let limit = 100
            for counter in 0...limit {
                // Use microsections to sleep the thread
                usleep(50000)
                
                dispatch_async(dispatch_get_main_queue()) {
                    if callback != nil {
                        let returnCode = callback?(0, counter / limit)
                        print("return something from the callback itself: \(returnCode)")
                    }
                }
            }
        }
    }
}
	