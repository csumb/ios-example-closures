//
//  ViewController.swift
//  Closures Example
//
//  Created by Jason Henderson on 9/22/15.
//  Copyright © 2015 CSU Monterey Bay. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var progressView: UIProgressView!
    
    @IBAction func runTest1(sender: UIButton) {
        progressView.setProgress(0, animated: true)
        
        let limit = 100
        for counter in 0...limit {
            // Use microsections to sleep the thread instead of seconds
            usleep(50000)
            
            self.progressView.setProgress(counter / limit, animated: true)
        }
    }
    
    @IBAction func runTest2(sender: UIButton) {

        // Reset the progress indicator
        progressView.setProgress(0, animated: true)
        
        // Do something long and asynchronous
        let doer = Doer()
        doer.doSomethingOnServer { (status: Int, progress: Float) -> Int in

            // Update the progress
            self.progressView.setProgress(progress, animated: true)

            // Return something. In this case, return doesn't make a whole lot of sense,
            // but if you needed something back in the calling context, you can pass it
            return 0
        }
        
        // Given this is an optional parameter, we don't have to provide anything, in which
        // case we just move on with whatever else we are doing
        //doSomethingOnServer(nil)
    }
    
    @IBAction func runTest3A(sender: UIButton) {
        runTest3(sender)
    }
    
    @IBAction func runTest3B(sender: UIButton) {
        runTest3(sender)
    }
    
    private func runTest3(sender: UIButton) {
        
        // Pull from user settings
        let showIncrements = (arc4random() % 2) == 0 ? true : false
        
        progressView.setProgress(0, animated: true)
        
        var updateProgressLabel:(Float, Float)->Void = {
            (progress:Float, step:Float) -> Void in
            
            if progress % step == 0 {
                self.progressView.setProgress(progress, animated: true)
            }
        }
        
        if !showIncrements {
            updateProgressLabel = {
                (progress:Float, step:Float) -> Void in
                
                self.progressView.setProgress(progress, animated: true)
            }
        }
        
        let priority = DISPATCH_QUEUE_PRIORITY_BACKGROUND
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            let limit = 100
            for counter in 0...limit {
                // Use microsections to sleep the thread
                usleep(50000)
                
                dispatch_async(dispatch_get_main_queue()) {
                    switch sender.tag {
                    case 1:
                        // Could we replace functionality for calculation with a closure variable?
                        updateProgressLabel(counter / limit, 0.10)
                    case 2:
                        updateProgressLabel(counter / limit, 0.01)
                    default:
                        print("No Tag...This is a Problem!")
                        //updateProgressLabel(counter / limit, 0.0)
                    }
                }
            }
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressView.setProgress(0, animated: true)
    }
}

